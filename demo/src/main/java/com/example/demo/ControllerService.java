package com.example.demo;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;





@RestController
public class ControllerService {

  private static final Logger LOGGER = LoggerFactory.getLogger(ControllerService.class);  


  
  /**
   * This method is responsible to append the list with its own contents.
   * 
   * @param Initial list
   * @return Appended list
   */
   @PostMapping("/append-list")
   @ResponseBody
   public List<Integer> appendList(@RequestParam List<Integer> inputList) {     
     
     LOGGER.info("Size fo inputList: "+inputList.size());
     if(!inputList.isEmpty()) {   	
    	 inputList.addAll(inputList);
     }     
     return inputList;
   }
   
   /**
    * This method is responsible to get the length of the audio file in seconds.
    * 
    * @param audio file name
    * @return Time in seconds
    */
   
   @PostMapping("/audio-length")
   @ResponseBody
   public float audioLength(@RequestParam String fileName) { 
	   
   File audioFile = null;
   float durationInSeconds  = 0;
   LOGGER.info("Source Audio file : "+fileName);
   try {
	   audioFile = new ClassPathResource(fileName).getFile();
	   AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(audioFile);
	   AudioFormat format = audioInputStream.getFormat();
	   long audioFileLength = audioFile.length();
	   int frameSize = format.getFrameSize();
	   float frameRate = format.getFrameRate();
	   durationInSeconds = Math.round((audioFileLength / (frameSize * frameRate)));
	} catch (UnsupportedAudioFileException | IOException e ) {
		e.printStackTrace();
	}	  
	   return  durationInSeconds;
   }
  
}
